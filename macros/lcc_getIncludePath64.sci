// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function lccincludepath = lcc_getIncludePath64()
  lccincludepath = [];
  if win64() then
    try
      lccincludepath =  winqueryreg('HKEY_CURRENT_USER', ..
                                    'Software\lcc\compiler64', ..
                                    'includepath');
    catch
      // remove last error on 'winqueryreg' fails
      lasterror();
      return;
    end
  end
endfunction
// =============================================================================
