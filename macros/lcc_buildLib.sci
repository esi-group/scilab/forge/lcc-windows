// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = lcc_buildLib(libexpname, destPath)
  bOK = %F;

  libraryname = fileparts(libexpname, 'fname');

  commandline = '""'+lcc_getBinPath() + filesep() +'buildLib"" ""' + libexpname + ..
                '"" ""'+ destPath + filesep() + libraryname + '.lib""';
  if ilib_verbose() == 2 then
    mprintf("%s\n", commandline);
    [msg, ierr] = unix_g(commandline);
    if ierr then
      mprintf("%s\n", msg(1));
    end
  else
    ierr = unix(commandline);
  end

  if ierr <> 0 then
    bOK = %F;
  else
    bOK = %T;
  end
endfunction
// =============================================================================
