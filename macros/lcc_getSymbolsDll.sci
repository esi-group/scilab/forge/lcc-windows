// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function symbols = lcc_getSymbolsDll(dllname, bExport)
  symbols = [];
  
  if win64() then
    warning('lcc_getSymbolsDll: Not supported on Windows 64 bit platform.');
    return
  end
  
  if ~isdef('bExport') then
    bExport = %T;
  end
  if isfile(dllname) then
    if bExport then
      commandline = """"+lcc_getBinPath() + filesep() + 'pedump.exe"" /EXP ' + dllname;
      [res, ierr] = unix_g(commandline);
      if ierr == 0 then
        s = size(res, '*');
        if s > 0 then
          symbols = list();
          symbols(1) = res(1);
          if s > 1 then
            symbols(2) = res(2:$);
          end
        end
      end
    else
      [res, ierr] = unix_g(""""+lcc_getBinPath() + filesep() + 'pedump.exe"" /I ' + dllname );
      if ierr == 0 then
        symbols = cleanImportTable(res);
      end
    end
  end
endfunction
// =============================================================================
function symbols = cleanImportTable(symbolsTable)
  symbols = [];

  symbolsTable(grep(symbolsTable, 'Imports of')) = [];
  symbolsTable(grep(symbolsTable, 'Imports Table')) = [];
  symbolsTable(grep(symbolsTable, 'OrigFirstThunk')) = [];
  symbolsTable(grep(symbolsTable, 'TimeDateStamp')) = [];
  symbolsTable(grep(symbolsTable, 'ForwarderChain')) = [];
  symbolsTable(grep(symbolsTable, 'First thunk RVA')) = [];
  symbolsTable(grep(symbolsTable, 'Ordn  Name')) = [];
  symbolsTable(symbolsTable == '') = [];
  [Ordrn, symbolsTable] = strtod(symbolsTable);
  symbolsTable = stripblanks(symbolsTable);
  dllNamesIndex = grep(symbolsTable, getdynlibext());

  if dllNamesIndex <> [] then
    sizedllNamesIndex = size(dllNamesIndex, '*');
    k = 1;
    symbols = list();
    for j = 1:sizedllNamesIndex
      symbols(k) = symbolsTable(dllNamesIndex(j));
      if j == sizedllNamesIndex then
        symbols(k + 1) = symbolsTable(dllNamesIndex(j) + 1:$);
      else
        symbols(k + 1) = symbolsTable(dllNamesIndex(j) + 1:dllNamesIndex(j + 1) - 1);
      end
      k = k + 2;
    end
  end
endfunction
// =============================================================================
