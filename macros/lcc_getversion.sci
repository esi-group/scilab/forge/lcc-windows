// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function lccver = lcc_getversion()
  lccver = '';
  try
    lccver =  winqueryreg('HKEY_CURRENT_USER', ..
                                   'Software\lcc', ..
                                   'Version');

  catch
    // remove last error on 'winqueryreg' fails
    lasterror();
    return;
  end
endfunction
// =============================================================================
