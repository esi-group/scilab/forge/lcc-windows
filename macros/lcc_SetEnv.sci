// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = lcc_SetEnv()
  bOK = %F;
  PATH = getenv('PATH', '');
  if PATH <> '' then

    PATH = lcc_getBinPath() + pathsep() + PATH;
    ierr = setenv('PATH', PATH);
    if ierr <> %T then
      return
    end

    LCCINCLUDE = getenv('LCCINCLUDE', '');
    LCCINCLUDE  = lcc_getIncludePath() + pathsep() + LCCINCLUDE;
    ierr = setenv('LCCINCLUDE', LCCINCLUDE);
    if ierr <> %T then
      return
    end

    LCCLIB = getenv('LCCLIB', '');
    LCCLIB  = lcc_getLibPath() + pathsep() + ..
              lcc_getLibPath() + pathsep() + LCCLIB;
    ierr = setenv('LCCLIB', LCCLIB);
    if ierr <> %T then
      return
    end

    bOK = %T;

  end
endfunction
// =============================================================================
