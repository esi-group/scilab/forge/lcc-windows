// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function lccbin = lcc_getBinPath()
  lccbin = [];
  if win64() then
    lccexe = 'lcc64.exe';
  else
    lccexe = 'lcc.exe';
  end
    
  lccroot = lcc_getRootPath();
  if lccroot <> [] then
    if isfile(lccroot + '\bin\' + lccexe) then
      lccbin = lccroot + '\bin';
    end
  end
endfunction
// =============================================================================
