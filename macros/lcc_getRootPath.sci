// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function lccroot = lcc_getRootPath()
  lccroot = [];
  if win64() then
    lccincludepath = lcc_getIncludePath64();
    lcclibpath = lcc_getLibPath64();
    if (lccincludepath <> []) & (lcclibpath <> []) then
      r1 = strncpy(lccincludepath, length(lccincludepath) - length('include64') - 1);
      r2 = strncpy(lcclibpath, length(lcclibpath) - length('lib64') - 1);
      if r1 == r2 then
        lccroot = r1;
      end 
    end
  else
    try  
      lccroot =  winqueryreg('HKEY_CURRENT_USER', ..
                             'Software\lcc', ..
                             'lccroot');
    catch
      // remove last error on 'winqueryreg' fails
      lasterror();
      return;
    end
  end
endfunction
// =============================================================================
