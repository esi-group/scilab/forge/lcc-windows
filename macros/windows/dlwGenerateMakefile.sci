// Copyright (C) DIGITEO - 2010 - Allan CORNET
// Copyright (C) Scilab Enterprises - 2012 - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//=============================================================================
function Makename = dlwGenerateMakefile(name, ..
                                 tables, ..
                                 files, ..
                                 libs, ..
                                 libname, ..
                                 with_gateway, ..
                                 ldflags, ..
                                 cflags, ..
                                 fflags, ..
                                 cc)

  [lhs,rhs] = argn(0);
  if rhs < 4 then
    error(msprintf(gettext("%s: Wrong number of input argument(s).\n"), "dlwGenerateMakefile"));
    return
  end

  if argn(2) < 6 then
    with_gateway = %t;
    ldflags = '';
    cflags = '';
    fflags = '';
    cc = '';
  end

  if ~isdef('makename') then
    makename = '';
  end

  for i=1:size(files,'*') // compatibility scilab 4.x
    [path_f, file_f, ext_f] = fileparts(files(i));
    if or(ext_f == ['.o','.obj']) then
      files(i) = path_f + file_f;
    else
      files(i) = path_f + file_f + ext_f;
    end
  end


  // change table if necessary
  if tables <> [] then
    if typeof(tables)<>'list' then
      tables = list(tables)
    end
    L = length(tables);

    for it = 1:L
      table = tables(it);
      [mt, nt] = size(table);

      if nt == 2 then
        col= "csci";
        table = [table, col(ones(mt,1))];
        nt=3;
      end

      if nt<>3 then
        error(msprintf(gettext("%s: Wrong size for input argument #%d.\n") ,"dlwGenerateMakefile", 2));
      end
      tables(it) = table;
    end
  end

  if isempty(makename) then
    Makename = dlwGetDefltMakefileName() + dlwGetMakefileExt();
  else
    Makename = makename + dlwGetMakefileExt();
  end

  if length(libname) > 0  & strncpy(libname, 3) <> 'lib' then 
    libname = 'lib' + libname;
  end

  ilib_gen_Make_lcc(name, tables, files, libs, libname, Makename, with_gateway, ldflags, cflags, fflags);

endfunction
//=============================================================================
function ilib_gen_Make_lcc(name, table, files, libs, libname, Makename, with_gateway, ldflags, cflags, fflags)
  managed_ext = ['.c', '.f'];
  obj_ext = ['.o', '.obj', ''];

  CFLAGS = cflags;
  MEXCFLAGS = '';
  FFLAGS = fflags;
  MEXFFLAGS = '';
  LDFLAGS = ldflags;
  SCIDIR = SCI;
  SCIDIR1 = pathconvert(SCI,%f,%f,'w');
  LCCLIBDIR = SCIHOME + filesep() + 'lcclib';
  FILES_SRC = '';
  OBJS = '';
  OTHERLIBS = '';

  if isempty(libname) then
    LIBRARY = name;
  else
    LIBRARY = libname;
  end


  FILES_SRC_MATRIX = [];

  [path_Make, file_Make, ext_Make] = fileparts(Makename);

  for i=1:size(files,'*')
    [path_f, file_f, ext_f] = fileparts(files(i));

    if or(obj_ext == ext_f) then
      FILENAME = [];
      FILE_FOUNDED = %f;
      for y = managed_ext(:)'
        if (FILE_FOUNDED == %f) then
          if (fileinfo(path_f + file_f + y) <> []) | (fileinfo(path_Make + file_f + y) <> []) then
            FILENAME = path_f + file_f + y;
            FILE_FOUNDED = %t;
          end
        end
      end
    else
      FILENAME = files(i);
    end
    FILES_SRC_MATRIX = [FILES_SRC_MATRIX , FILENAME];
  end

  if typeof(tables) <> 'list' then
    tables = list(tables);
  end
  L = length(tables);

  if with_gateway then
    if L == 1 then
      FILES_SRC_MATRIX = [FILES_SRC_MATRIX , name + '.c'];
    else
       for i=1:L
        FILES_SRC_MATRIX = [FILES_SRC_MATRIX , name + string(i) + '.c'];
       end
    end
  end

  for it=1:L
    table = tables(it);
    [mt,nt] = size(table);

    for i=1:mt ;
      // mex files to be added
      if table(i,3)=='cmex' | table(i,3)=='fmex' | table(i,3)=='Fmex'
        FILES_SRC_MATRIX = [FILES_SRC_MATRIX , table(i,2)];
      end
    end
  end

  // remove duplicate files
  FILES_SRC_MATRIX = unique(FILES_SRC_MATRIX);

  FILES_SRC = strcat(FILES_SRC_MATRIX,' ');

  if table(i,3)=='cmex' | table(i,3)=='fmex' | table(i,3)=='Fmex' then
    MEXCFLAGS = "-Dmexfunction_=mex$*_ -DmexFunction=mex_$*";
    MEXFFLAGS = "-Dmexfunction=mex$*";
  end

  OBJS_MATRIX = [];

  for y = 1:size(FILES_SRC_MATRIX,'*')
    [path_f, file_f, ext_f] = fileparts(FILES_SRC_MATRIX(y));
    OBJS_MATRIX = [OBJS_MATRIX, path_f + file_f + '.obj'];
  end

  OBJS = strcat(OBJS_MATRIX, ' ');

  for x=libs(:)'
     if (x <> [] & x <> '') then
       if OTHERLIBS <> '' then
         OTHERLIBS = OTHERLIBS + ' ' + x + '.lib';
       else
         OTHERLIBS = x + '.lib';
       end
     end
  end
  OTHERLIBS = strsubst(OTHERLIBS,'/',filesep());
  [macros, pathlib] = libraryinfo('lcc_windowslib');
  pathscript = fullfile(pathlib, '../script/TEMPLATE_MAKEFILE.LCC');
  ierr = execstr("MAKEFILE_LCC = mgetl(''" + pathscript + "'');", "errcatch");
  if ierr <> 0 then
    MAKEFILE_LCC = '';
    warning(pathscript + ' ' + _('not found.') );
  else
    if win64() then
      MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LCC__" , "lcc64");
      MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LCCLNK__" , "lcclnk64");
    else
      MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LCC__" , "lcc");
      MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LCCLNK__" , "lcclnk");
    end
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LDFLAGS__" , LDFLAGS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__SCI__" , SCIDIR);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__SCIDIR1__" , SCIDIR1);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LCCLIBDIR__" , LCCLIBDIR);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__LIBRARY__" , LIBRARY);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__FILES_SRC__" , FILES_SRC);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__OTHERSLIBS__" , OTHERLIBS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__CFLAGS__" , CFLAGS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__MEXCFLAGS__" , MEXCFLAGS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__FFLAGS__" , FFLAGS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__MEXFFLAGS__" , MEXFFLAGS);
    MAKEFILE_LCC = strsubst(MAKEFILE_LCC , "__OBJS__" , OBJS);

    mputl(MAKEFILE_LCC, Makename);

    if ilib_verbose() > 1 then
      disp(mgetl(Makename));
    end

  end

endfunction
//=============================================================================
