// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
// Copyright (C) Scilab Enterprises - 2012 - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = lcc_convertScilabLibs()
  bOK = %T;
  scilabDlls = ['blasplus', ..
                'lapack', ..
                'MALLOC', ..
                'libf2c', ..
                'core', ..
                'core_f', ..
                'intersci', ..
                'output_stream', ..
                'dynamic_link', ..
                'integer', ..
                'optimization_f', ..
                'libjvm', ..
                'scilocalization', ..
                'libintl', ..
                'linpack_f', ..
                'call_scilab', ..
                'time', ..
                'api_scilab', ..
                'scilab_windows'];

  if with_module('scicos') then
    scilabDlls = [scilabDlls, ..
                 'scicos', ..
                 'scicos_f', ..
                 'scicos_blocks', ..
                 'scicos_blocks_f'];
  end

  nbScilabDlls = size(scilabDlls, '*');
  
  // LCC - Windows 64 bits did not require conversion then only a copy
  if win64() then
    scilabDlls = [scilabDlls, 'libscilab'];
    nbScilabDlls = size(scilabDlls, '*');
    for j = 1:nbScilabDlls
      copyfile(SCI + filesep() + 'bin' + filesep() + scilabDlls(j) + '.lib', lcc_getScilabLibPath());
    end
    return
  end

  if ( ilib_verbose() <> 0 ) then
    mprintf('Converting Libraries.\n');
  end

  for j = 1:nbScilabDlls

    if ( ilib_verbose() <> 0 ) then
      mprintf(gettext('Build %s.lib\n'), scilabDlls(j));
    end

    ok = lcc_convertLibrary(SCI + filesep() + 'bin' + filesep() + scilabDlls(j) + getdynlibext(), lcc_getScilabLibPath());

    if ~ok then
      if ( ilib_verbose() <> 0 ) then
        mprintf(gettext('Conversion failed %s.lib\n'), scilabDlls(j));
      end
      bOK = %F;
    end
  end

endfunction
// =============================================================================
