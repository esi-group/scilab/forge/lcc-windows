changelog of the lcc-windows

lcc-windows (0.5.2) 
    * Bug in dlwGenerateMakefile.sci fixed (wrong libname).

 -- Vincent Couvert <vincent.couvert@scilab-enterprises.com>


lcc-windows (0.5.1) 
    * Update for Scilab 5.4.1

 -- Vincent Couvert <vincent.couvert@scilab-enterprises.com>


lcc-windows (0.5) 
    * Update for Scilab 5.4.0
    * Issue #954 fixed - Variable dlwGetXcosIncludes was not defined.

 -- Vincent Couvert <vincent.couvert@scilab-enterprises.com>


lcc-windows (0.4) 
    * Bug #8879 fixed - http://bugzilla.scilab.org/show_bug.cgi?id=8879
      (Thanks to Guillaume Jacquenot)
    * Bug #8880 fixed - http://bugzilla.scilab.org/show_bug.cgi?id=8880

 -- Allan Cornet <allan.cornet@scilab.org>


lcc-windows (0.3) 
    * Fix bug about wrong path for makefile generation

 -- Allan Cornet <allan.cornet@scilab.org>


lcc-windows (0.2) 
    * Fix some bugs (updated with 5.3-beta2)

 -- Allan Cornet <allan.cornet@scilab.org>


lcc-windows (0.1) - 31/05/2010
    * ATOMS version
    * LCC-Win32 (Windows 32 bits)
      64 bits version will be supported quickly.

 -- Allan Cornet <allan.cornet@scilab.org>

